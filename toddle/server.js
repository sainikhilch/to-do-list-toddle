const express = require('express');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const path = require('path');
const taskRoutes = require('./taskRoutes');
const db = require('./db');

const app = express();
const PORT = process.env.PORT || 8000;

// Middleware
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

// Passport Configuration
passport.use(new LocalStrategy(
    function(username, password, done) {
        db.query('SELECT * FROM users WHERE username = ?', [username], (err, results) => {
            if (err) return done(err);
            if (!results.length) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            const user = results[0];
            bcrypt.compare(password, user.password, (err, isMatch) => {
                if (err) return done(err);
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, { message: 'Incorrect password.' });
                }
            });
        });
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    db.query('SELECT * FROM users WHERE id = ?', [id], (err, results) => {
        if (err) return done(err);
        done(null, results[0]);
    });
});

// CRUD operations for tasks
app.post('/tasks', (req, res) => {
    const { text, dateofissue, priority, tasktype } = req.body;
    const task = { text, dateofissue, priority, tasktype };
    db.query('INSERT INTO tasks SET ?', task, (err, result) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error adding task');
        } else {
            console.log('Task added successfully');
            res.send('Task added successfully');
        }
    });
});

app.get('/tasks/:id', (req, res) => {
    const taskId = req.params.id;
    db.query('SELECT * FROM tasks WHERE id = ?', [taskId], (err, result) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error retrieving task');
        } else {
            if (result.length === 0) {
                res.status(404).send('Task not found');
            } else {
                res.json(result[0]);
            }
        }
    });
});

app.put('/tasks/:id', (req, res) => {
    const taskId = req.params.id;
    const { text, dateofissue, priority, tasktype } = req.body;
    db.query('UPDATE tasks SET text = ?, dateofissue = ?, priority = ?, tasktype = ? WHERE id = ?', [text, dateofissue, priority, tasktype, taskId], (err, result) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error updating task');
        } else {
            console.log('Task updated successfully');
            res.send('Task updated successfully');
        }
    });
});

app.delete('/tasks/:id', (req, res) => {
    const taskId = req.params.id;
    db.query('DELETE FROM tasks WHERE id = ?', [taskId], (err, result) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error deleting task');
        } else {
            console.log('Task deleted successfully');
            res.send('Task deleted successfully');
        }
    });
});

// Routes
app.use('/', taskRoutes);
app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'login.html'));
});

// Define route handler for GET request to /register
app.get('/register', (req, res) => {
    // Render your registration page here or send any response
    res.sendFile(path.join(__dirname, 'public', 'register.html'));
});


app.post('/register', (req, res) => {
    const { username, email, password } = req.body;
    bcrypt.hash(password, 10, (err, hash) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error registering user');
        } else {
            db.query('INSERT INTO users (username, email, password) VALUES (?, ?, ?)', [username, email, hash], (err, result) => {
                if (err) {
                    console.error(err);
                    res.status(500).send('Error registering user');
                } else {
                    console.log('User registered successfully');
                    res.redirect('/login');
                }
            });
        }
    });
});

// app.post('/login', passport.authenticate('local', {
//     successRedirect: '/home',
//     failureRedirect: '/login',
//     failureFlash: true
// }));

app.post('/login', 
    passport.authenticate('local', { 
        successRedirect: '/index.html',  // Redirect to index.html on successful login
        failureRedirect: '/login',       // Redirect back to login page on failure
        failureFlash: true               // Enable flashing of error messages
    })
);

app.get('/home', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});
