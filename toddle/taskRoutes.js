const express = require('express');
const router = express.Router();
const db = require('./db'); // Import MySQL connection

router.post('/tasks', (req, res) => {
    const { text, dateofissue, priority, tasktype } = req.body;
    const task = { text, dateofissue, priority, tasktype };
    db.query('INSERT INTO tasks SET ?', task, (err, result) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error adding task');
        } else {
            console.log('Task added successfully');
            res.send('Task added successfully');
        }
    });
});

router.get('/tasks/:id', (req, res) => {
    const taskId = req.params.id;
    db.query('SELECT * FROM tasks WHERE id = ?', [taskId], (err, result) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error retrieving task');
        } else {
            if (result.length === 0) {
                res.status(404).send('Task not found');
            } else {
                res.json(result[0]);
            }
        }
    });
});

router.put('/tasks/:id', (req, res) => {
    const taskId = req.params.id;
    const { text, dateofissue, priority, tasktype } = req.body;
    db.query('UPDATE tasks SET text = ?, dateofissue = ?, priority = ?, tasktype = ? WHERE id = ?', [text, dateofissue, priority, tasktype, taskId], (err, result) => {
        if (err) {
            console.error(err);
            res.status(500).send('Error updating task');
        } else {
            console.log('Task updated successfully');
            res.send('Task updated successfully');
        }
    });
});

module.exports = router;
